# UniverSIS Documentation

  An open source Student Information System to manage your academic processes, supported by a collaborative community.

 > Official website: https://www.universis.gr/

 > Open source project website: https://universis.io/

## Main Features - Γενικά στοιχεία εφαρμογών
  Γενικές πληροφορίες για τα features των εφαρμογών, κατάλληλες για support teams και δυνητικά για τελικούς χρήστες.

  * [Students Application](gr/students-features.md)
  * [Faculty Application](gr/registrar-features.md)
  * [Registrar Application](gr/registrar-features.md)

## Developers
  ### [Effective Statuses](gr/effectiveStatus.md ':include')
