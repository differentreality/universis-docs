# Effective Statuses
Το document αυτό αναφέρεται στο flow που μας έχει δώσει η UX Team για τις δηλώσεις:
https://docs.google.com/document/d/1LoPLM1N-FOeZPgxQXu-K-1r3Awl1rNUSQTn2rxfhvpU/edit?usp=sharing
και στα effective statuses που επιστρέφει το API: https://sis-app.dev.duth.gr/v2/api/PeriodRegistrationEffectiveStatuses
κάνοντας μια προσπάθεια να αντιστοιχισει το πρώτο με το δεύτερο, καθώς σε κάποια σημεία οι δυνατότητες του API δεν συμβαδίζουν με το design.

Οι 3 πρωτες στήλες του πίνακα είναι τα στοιχεία που επιστρέφονται για καθένα από τα effective statuses. Η τέταρτη στήλη είναι η κατηγορία στην οποία κατατάσεται το status αυτό σύμφωνα με το πρωτο εγγραφο και καθορίζει το ποια οθόνη θα εμφανιστεί αρχικά, ενω η στήλη no. μας δίνει τον συγκεκριμένο αριθμό στην λίστα ΠΕΡΙΠΤΩΣΕΙΣ του εγγραφου(δηλαδη INITIAL VIEW + 3 είναι το 3ο στοιχειο στην λιστα ΠΕΡΙΠΤΩΣΕΙΣ του INITIAL VIEW στο εγγραφο της UX).
Στην στήλη screen υπαρχει ενα λινκ για την ακριβή οθονη στο invision ενω η τελευταία περιέχει TRUE/FALSE ανάλογα με το αν μπορεί να γίνει edit της δήλωσης(Ο πινακας αληθειας που είχαμε πει).

*H περίπτωση με id 2 αφορά την δήλωση εξαμήνου για αυτό δεν αναφέρεται.

Εκτός από αυτό, στο documentation της UX έχω επίσης κάνει highlight καποιες περιπτώσεις που δεν έχω βρει πως καλύπτονται απο το API, οπότε αν όντως δεν καλύπτονται να αλλάξουν/διαγραφουν για να υπάρχει εννιαίο documentaiton, αλλίως να το διορθώσουμε.

| #  | code                         | status | stage        | No      | screen | can edit? |
|----|------------------------------|--------|--------------|---------|--------|-----------|
| 1  | OPEN_NO_TRANSACTION          | OPEN   | INITIAL VIEW | 3       | [321515952](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515952/preview)  | TRUE      |
| 2  | CLOSED_NO_TRANSACTION        | CLOSED | INITIAL VIEW | 2, 56   | [321515957](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515957/preview) <br>[321515956](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515956/preview)  | FALSE     |
| 3  | CLOSED_NO_REGISTRATION       | CLOSED | INITIAL VIEW | 2, 56   | [321515957](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515957/preview) <br> [321515956](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515956/preview)  | FALSE     |
| 4  | SELECT_SPECIALTY             | OPEN   | INITIAL VIEW | 4       | [321515955](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515955/preview)  | FALSE     |
| 5  | PENDING_TRANSACTION          | OPEN   | OVERVIEW     | 3       | [321835843](https://projects.invisionapp.com/d/main/default/#/console/14789607/321835843/preview)       | TRUE      |
| 6  | SUCCEDED_TRANSACTION         | OPEN   | OVERVIEW     | 4       | [321835844](https://projects.invisionapp.com/d/main/default/#/console/14789607/321835844/preview)       | TRUE      |
| 7  | FAILED_TRANSACTION           | OPEN   | OVERVIEW     | ?       | ?      | TRUE      |
| 8  | P_SUCCEEDED_TRANSACTION      | OPEN   | OVERVIEW     | 6       | [321835845](https://projects.invisionapp.com/d/main/default/#/console/14789607/321835845/preview)       | TRUE      |
| 9  | P_SYSTEM_TRANSACTION         | OPEN   | OVERVIEW     | ?       | ?      | TRUE      |
| 10 | CLOED_REGISTRATION_PERIOD    | CLOSED | INITIAL VIEW | 2, 5, 6 | [321515957](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515957/preview) <br> [321515956](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515956/preview)      | FALSE     |
| 11 | CLOSED_PERIOD_NO_TRANSACTION | CLOSED | INITIAL VIEW | 2, 5, 6 | [321515957](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515957/preview) <br> [321515956](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515956/preview)      | FALSE     |
| 12 | INACTIVE_STUDENT             | CLOSED | INITIAL VIEW | 1       | [321515960](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515960/preview)       | FALSE      |
| 13 | CLOSED_REGISTRATION          | CLOSED | INITIAL VIEW | 2, 5, 6 | [321515959](https://projects.invisionapp.com/d/main/default/#/console/14789607/321515959/preview)       | FALSE     |
