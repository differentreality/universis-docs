# Documentation for UniverSIS project

> https://gitlab.com/universis

Created with [docsify](https://docsify.js.org/#/quickstart)

## Run

`docsify serve docs`

and see it running at

http://localhost:3000

## How to write documentation

Write your content in markdown! 

* Quick guide: https://www.markdownguide.org/cheat-sheet/
* More details: https://www.markdownguide.org/basic-syntax/ and https://www.markdownguide.org/extended-syntax/